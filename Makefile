REPORT="report.txt"
RULE="all"

compare: install
	./compare.sh $(REPORT) $(RULE)

install:
	./install.sh

clean:
	rm -rf report.txt* env
